jQuery(document).ready(function () {
  var rndNum = Math.ceil(Math.random() * 5);
  jQuery("body").not(".home").css({
    "background-image": "url(/images/img" + rndNum + ".webp)",
  });

  $("ul#menul").on("click", function () {
    // $("ul#menul > li").toggle("fast");
    $(this).toggleClass("open");
  });
});
